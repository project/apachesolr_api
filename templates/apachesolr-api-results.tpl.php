<?php

/**
 * @file
 * Template file for search results.
 */
?>
<h2><?php echo $meta['title']; ?></h2>
<div class="search-info">
  <span class="total-rows"><?php echo $meta['total_rows']; ?></span> results for <span class="query"><?php echo $meta['query']; ?></span>
</div>
<hr>
<div class="search-results">
  <?php foreach($rows as $row): ?>
    <div class="search-result-item">
      <a href="<?php echo $row['url'] ?>">
        <div class="label"><?php echo $row['label'] ?></div>
        <div class="content"><?php echo $row['content'] ?></div>
      </a>
      <br>
    </div>
  <?php endforeach; ?>
</div>
<?php print $pager; ?>
