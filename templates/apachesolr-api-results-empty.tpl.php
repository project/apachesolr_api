<?php

/**
 * @file
 * Template file for Empty results.
 */
?>
<h2><?php echo $meta['title']; ?></h2>
<div class="search-info">
  Your search <span class="query"><?php echo $meta['query']; ?></span>
</div>
