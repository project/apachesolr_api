Apache Solr API

Helper module for developers to interact with apachesolr backend in Drupal.

## Usage

Implement hook_apachesolr_api_config() in your module.
It is recommended to declare dependency on yaml_parser and pick up configuration from a yaml file as shown in the example below.
```
/**
 * Implements hook_apachesolr_api_config().
 */
function mymodule_search_apachesolr_api_config() {
  $data = array();
  $path = drupal_get_path('module', 'mymodule_search');
  if ($path) {
    $filepath = $path . '/apachesolr_api_config.yaml';
    $realpath = drupal_realpath($filepath);
    $yaml = file_get_contents($filepath);
    $data = yaml_parser_parse_string($yaml);
  }
  return $data;
}
```

A sample yaml file could look like as follows,
```
global_search_url: 'sitewide-search'
panelized content types:
  - article
  - page
  - resource
pages:
  search_detail_page:
    url: search-results/%
    menu_item:
      title: Search results
      page callback: 'apachesolr_api_search_page_callback'
      page arguments:
        - search_detail_page
        - 1
      access arguments:
        - access content
    apachesolr:
      fuzzy: FALSE
      params:
        fl: "id, label, content, teaser, entity_id, bundle, entity_type, path, url"
        hl: TRUE
        hl.fl: "content, label"
        rows: 10
        hl.snippets: 1
        hl.fragsize: 300
        hl.simple.pre: "<mark>"
        hl.simple.post: "</mark>"
        hl.simple.snippets: TRUE
        qf:
         - "label^20"
         - "content^5"
      map:
        label: label
        content: content
        url: path
  autocomplete:
    url: ajax/mymodule-search/%
    menu_item:
      title: Search Suggestions
      page callback: 'apachesolr_api_search_page_callback'
      page arguments:
        - autocomplete
        - 2
      access arguments:
        - access content
    apachesolr:
      fuzzy: FALSE
      params:
        fl: "id, label, content, teaser, entity_id, bundle, entity_type, path, url"
        hl: TRUE
        hl.fl: "content, label"
        rows: 10
        hl.simple.pre: "<mark>"
        hl.simple.post: "</mark>"
        hl.simple.snippets: TRUE
        qf:
         - "label^20"
         - "content^5"
      map:
        label: label
        content: content
        url: path
  bookmarked_and_liked:
    apachesolr:
      fuzzy: FALSE
      params:
        fl: "entity_id id"
        rows: 4
        fq:
          - bundle:resource
      map:
        nid: entity_id
flag:
  save_for_later:
    entity:
      node:
        bundle:
         - resource
    flag_machine_name: save_for_later
  recommend:
    entity:
      node:
        bundle:
         - resource
    flag_machine_name: like
custom_facets:
  like:
    label: My likes
    type: like
    solr_field: is_flag_like
    category: current_user
  bookmark:
    label: My bookmarks
    type: bookmark
    solr_field: im_flag_save_for_later
    category: current_user

```

## Configuration Parameters

### global_search_url
apachesolr_api provides a form (apachesolr_api_form) for sitewide search implementation.
The search page url can be passed through `global_search_url`.

### panelized content types
Since the panelized nodes don't store their values as fields attached to nodes, we have to do some extra processing to get them indexed.
Pass a list of panelized content types where such processing is required.

### pages
Depending on the parameters passed, it would create full search pages, autocomplete path or just simply a config that holds apachesolr parameters for custom queries.
`url` along with `menu_item` would be used to create a hook_menu entry.
`menu_item` will be passed to hook_menu item declaration as is.
`apachesolr` holds the configuration for the query to be executed.
`apachesolr.fuzzy` if set to true, it will support fuzzy logic on query supplied.
`apachesolr.params` will be merged with custom query. Refer to apachesolr documentation for supported parameters.
`map` would be used to preprocess the results so that fieldnames in apachesolr index would be replaced with custom ones.
Example configuration
```
search_detail_page:
    url: search-results/%
    menu_item:
      title: Search results
      page callback: 'apachesolr_api_search_page_callback'
      page arguments:
        - search_detail_page
        - 1
      access arguments:
        - access content
    apachesolr:
      fuzzy: FALSE
      params:
        fl: "id, label, content, teaser, entity_id, bundle, entity_type, path, url"
        hl: TRUE
        hl.fl: "content, label"
        rows: 10
        hl.snippets: 1
        hl.fragsize: 300
        hl.simple.pre: "<mark>"
        hl.simple.post: "</mark>"
        hl.simple.snippets: TRUE
        qf:
         - "label^20"
         - "content^5"
      map:
        label: label
        content: content
        url: path
```
NOTE:

- For a full page display one could make use of `apachesolr_api_search_page_callback` callback provided by the module.
    - Supported types are `autocomplete` and `search_detail_page`. This has to be supplied as first parameter in the `page arguments` for menu_item.
    - `search_detail_page` Templates associated with the ouptputs are `apachesolr-api-results.tpl.php` and `apachesolr-api-results-empty.tpl.php`.
    - `autocomplete` will use `drupal_json_output` to give back results.
- You could also use the helper function `apachesolr_api_fetch` to get the results directly.
Eg.
```
global $user;
$config_type = 'bookmarked_and_liked';
$query = '*';
$params = [
  'sort' => 'ds_field_mi_created_date asc',
  'fq' => ['im_flag_recommend:' . $user->uid],
];
$solr_results = apachesolr_api_fetch($config_type, $query, $params);
```

### flag
Flag module interactions are not stored on fields attached to entities. If we need to query results based on flaged status, we would need to index them.
Flag data will be attached to the entity with custom fields with following prefixes,
- `is_flag_` : Single valued field which will hold total flag count.
- `im_flag_` : Multivalued field that holds user id of each user who has flagged the content.
Eg.
```
flag:
  save_for_later:
    entity:
      node:
        bundle:
         - resource
    flag_machine_name: save_for_later
```

### custom_facets
Custom fields added to document index won't expose facets like fields do. Use this configuration to create pseudo facet links for such fields.
`apachesolr_api_get_current_user_flagged_filters()` will provide you with a list of such pseudo-facet links.
Eg.
```
custom_facets:
  like:
    label: My likes
    type: like
    solr_field: is_flag_like
    category: current_user
```