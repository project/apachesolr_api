<?php

/**
 * @file
 * Filters.
 */

/**
 * Collect filters that can be used for current configuration.
 */
function apachesolr_api_filters_list($c, $query_params, $path, $results, $unfiltered_results) {
  $config = $c['apachesolr'];
  $filters = [];
  // Interpret boolean filters.
  if (!empty($config['filters']['query']) && is_array($config['filters']['query'])) {
    foreach ($config['filters']['query'] as $key => $filter_config) {
      $filters['query'][$key] = _apachesolr_api_filter_helper_get_item($query_params, $filter_config['query_string'], '1', FALSE, $path);
    }
  }

  // Interpret Facet Query filters.
  // @todo test this.
  if (!empty($config['filters']['facet_queries']) && is_array($config['filters']['facet_queries'])) {
    foreach ($config['filters']['facet_queries'] as $key => $filter_config) {
      $filters['facet_query'][$key] = _apachesolr_api_filter_helper_get_item($query_params, $filter_config['query_string'], '1', FALSE, $path);
    }
  }

  // Interpret Facet Query filters.
  if (!empty($config['filters']['facet_fields']) && is_array($config['filters']['facet_fields'])) {
    foreach ($config['filters']['facet_fields'] as $key => $filter_config) {
      $facet_lists = [];
      $initial_facet_list = $current_facet_list = NULL;
      if (isset($unfiltered_results['meta']['response']->facet_counts->facet_fields->{$filter_config['solr_field']})) {
        $initial_facet_list = $unfiltered_results['meta']['response']->facet_counts->facet_fields->{$filter_config['solr_field']};
      }
      if (isset($results['meta']['response']->facet_counts->facet_fields->{$filter_config['solr_field']})) {
        $current_facet_list = $results['meta']['response']->facet_counts->facet_fields->{$filter_config['solr_field']};
      }
      if ($initial_facet_list) {
        foreach ($initial_facet_list as $k => $v) {
          $decoded_value = _apachesolr_api_filter_decode($filter_config['field_type'], $k);
          $facet_lists[$k] = _apachesolr_api_filter_helper_get_item($query_params, $filter_config['query_string'], $decoded_value, TRUE, $path);
          $facet_lists[$k] += [
            'total' => $v,
            'current' => isset($current_facet_list->{$k}) ? $current_facet_list->{$k} : 0,
            'value' => $decoded_value,
          ];
        }
      }
      $filters['facet_field'][$key] = $facet_lists;
    }
  }
  return $filters;
}

/**
 * Collect active filters and adjust params.
 */
function apachesolr_api_filters_process_parameters($c, &$params, $query_params, $path) {
  $config = $c['apachesolr'];
  // Interpret boolean filters.
  if (!empty($config['filters']['query']) && is_array($config['filters']['query'])) {
    foreach ($config['filters']['query'] as $filter_config) {
      $f = _apachesolr_api_filter_helper_get_item($query_params, $filter_config['query_string'], '1', FALSE, $path);
      if ($f['status']) {
        $params['fq'][] = $filter_config['solr_query'];
      }
    }
  }

  // Interpret Facet Query filters.
  // @todo test this.
  if (!empty($config['filters']['facet_queries']) && is_array($config['filters']['facet_queries'])) {
    foreach ($config['filters']['facet_queries'] as $filter_config) {
      $f = _apachesolr_api_filter_helper_get_item($query_params, $filter_config['query_string'], '1', FALSE, $path);
      if ($f['status']) {
        $params['fq'][] = $filter_config['solr_query'];
      }
    }
  }

  // Interpret Facet Field filters.
  if (!empty($config['filters']['facet_fields']) && is_array($config['filters']['facet_fields'])) {
    foreach ($config['filters']['facet_fields'] as $filter_config) {
      $logic = isset($filter_config['multiple_behaviour']) ? $filter_config['multiple_behaviour'] : 'OR';
      $concatenate_logic = ' ' . strtoupper(trim($logic)) . ' ';

      $field_values = [];
      $active_filter_values = [];
      if (!empty($query_params[$filter_config['query_string']]) && $active_filter_values = $query_params[$filter_config['query_string']]) {
        foreach ($active_filter_values as $field_value) {
          $field_values[] = _apachesolr_api_filter_encode($filter_config['field_type'], $field_value);
        }
      }
      if (!empty($field_values)) {
        $params['fq'][] = $filter_config['solr_field'] . ':(' . implode($concatenate_logic, $field_values) . ')';
      }
    }
  }
}

/**
 * Formats a filter item and returns it.
 */
function _apachesolr_api_filter_helper_get_item($filters, $key, $value, $is_multiple, $path) {
  $query_params = $filters;
  $is_active = FALSE;
  if ($is_multiple) {
    if (!empty($query_params[$key])) {
      foreach ($query_params[$key] as $k => $v) {
        if ($v == $value) {
          $is_active = TRUE;
          unset($query_params[$key][$k]);
          break;
        }
      }
    }
    if (!$is_active) {
      $query_params[$key][] = $value;
    }
  }
  else {
    if (!empty($filters[$key])) {
      $is_active = TRUE;
      unset($query_params[$key]);
    }
    else {
      $query_params[$key] = $value;
    }
  }

  // Remove a pager if present.
  unset($query_params['pager']);

  $facet_link = url($path, array('query' => $query_params));

  $item = [
    'status' => $is_active,
    'url' => $facet_link,
  ];
  return $item;
}

/**
 * Format filter value based on field type.
 */
function _apachesolr_api_filter_encode($type, $value) {
  $value = trim($value);
  $formatted_value = $value;
  switch ($type) {
    case 'taxonomy_term':
      $formatted_value = '"taxonomy_term:' . $value . '"';
      break;

    case 'boolean':
      $formatted_value = $value ? 'true' : 'false';
      break;

  }

  return $formatted_value;
}

/**
 * Format filter value based on field type.
 */
function _apachesolr_api_filter_decode($type, $value) {
  $value = trim($value);
  $formatted_value = $value;
  switch ($type) {
    case 'taxonomy_term':
      $formatted_value = str_replace('taxonomy_term:', '', $value);
      break;

    case 'boolean':
      $formatted_value = $value == 'true' ? TRUE : FALSE;
      break;

  }

  return $formatted_value;
}
