<?php

/**
 * @file
 * Describe hooks provided by the Apachesolr API module.
 */

/**
 * Describes config data for Apachesolr API.
 *
 * @return array
 *   An associative array describing the config structure.
 */
function hook_apachesolr_api_config() {
  $config = [];
  
  // Configure the site-wide search url.
  $config['global_search_url'] = 'sitewide-search';

  // Provide list of panelized content types, so that they can be indexed properly.
  $config['panelized content types'] = [
    'article',
    'page',
  ];
    
  // Create pages that display content from index.
  $config['pages']['bookmarked'] = [
    'apachesolr' => [
      'fuzzy' => FALSE,
      // Parameters that has to be passed to apache solr query.
      'params' => [
        'fl' => 'id entity_id',
        'rows' => 4,
      ],
      // Map results from apachesolr to custom field names.
      'map' => [
        'nid' => 'entity_id',
      ],
    ],      
  ];
    
  // Pass Flag information.
  $config['flag']['liked'] = [
    // Entity on which flag is attached to.
    // @todo Support other entities than node.
    'entity' => [ 
      'node' => [ 
        'bundle' => [
          'page',
          'article',
        ]
      ]
    ],
    // Machine name of the flag.
    'flag_machine_name' => 'like'
  ];

  // Custom Facets handling.
  $config['custom_facets']['like'] = [
    'label' => 'My Likes',
    'type' => 'like',
    'solr_field' => 'is_flag_like',
    'category' => 'current_user'
  ];
  return $config;
}
