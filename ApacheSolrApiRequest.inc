<?php

/**
 * @file
 * ApacheSolrApiRequest Class.
 */

/**
 *
 */
class ApacheSolrApiRequest {

  private $config;
  private $configType;
  private $query = '*';
  private $inputParams = [];
  private $queryParams = NULL;
  private $path = NULL;
  private $placeholders = [];

  /**
   * Constructor.
   *
   * @param array $config
   *   A structured array of configurations.
   */
  public function __construct($configuration = []) {
    if (isset($configuration['config_type'])) {
      $this->setConfigType($configuration['config_type']);
      $config = apachesolr_api_load_config();
      $c = $config['pages'][$this->getConfigType()];
      $this->setConfig($c);
    }
    if (isset($configuration['query'])) {
      $this->setQuery($configuration['query']);
    }
    if (isset($configuration['input_params'])) {
      $this->setInputParams($configuration['input_params']);
    }
    if (isset($configuration['query_params'])) {
      $this->setQueryParams($configuration['query_params']);
    }
    if (isset($configuration['path'])) {
      $this->setPath($configuration['path']);
    }
    if (isset($configuration['placeholders'])) {
      $this->setPlaceholders($configuration['placeholders']);
    }
  }

  /**
   * Fetch.
   */
  public function fetch() {
    module_load_include('inc', 'apachesolr_api', 'filters');
    $c = $this->replaceConfigPlaceholders($this->getConfig(), $this->getPlaceholders());
    $query_params = $this->getQueryParams() ? $this->getQueryParams() : drupal_get_query_parameters();
    $path = $this->getPath() ? $this->getPath() : current_path();
    $params = $this->getInputParams();
    $query = $this->getQuery();
    $this->processPaginationParameters($params, $query_params);
    apachesolr_api_filters_process_parameters($c, $params, $query_params, $path);
    $results = _apachesolr_api_fetch($c, $query, $params);
    _apachesolr_api_pagination_process_results($c, $query, $params, $path, $results);

    // Do a unfiltered query for meta data retrieval.
    $params = $this->getInputParams();
    $this->processPaginationParameters($params, $query_params);
    $unfiltered_results = _apachesolr_api_fetch($c, $query, $params);
    $results['meta']['unfiltered'] = $unfiltered_results;
    $filters = apachesolr_api_filters_list($c, $query_params, $path, $results, $unfiltered_results);

    $results['filters'] = $filters;
    return $results;
  }

  /**
   * Do placeholder replacements in the query.
   */
  private function replaceConfigPlaceholders($config, $placeholders) {
    foreach ($placeholders as $placeholder => $value) {
      if (!empty($config['apachesolr']['params']['fq'])) {
        foreach ($config['apachesolr']['params']['fq'] as &$fq) {
          $fq = str_replace($placeholder, $value, $fq);
        }
      }
    }
    return $config;
  }

  /**
   * Helper function to process pagination parameters.
   */
  private function processPaginationParameters(&$params, $filters, $reset = FALSE) {
    $config = $this->getConfig();
    if (empty($config['apachesolr']['pagination'])) {
      return;
    }
    $pagination_config = $config['apachesolr']['pagination'];
    if ($pagination_config['type'] == 'cursors') {
      if ($reset) {
        $params['cursorMark'] = '*';
      }
      else {
        $query_string = isset($pagination_config['type']['query_string']) ? $pagination_config['type']['query_string'] : 'cursorMark';
        $params['cursorMark'] = isset($filters[$query_string]) ? $filters[$query_string] : '*';
      }
    }
  }

  /**
   * Set configType.
   */
  public function setConfigType($configType) {
    $this->configType = $configType;
  }

  /**
   * Get configType.
   */
  public function getConfigType() {
    return $this->configType;
  }

  /**
   * Set query.
   */
  public function setQuery($query) {
    $this->query = $query;
  }

  /**
   * Get query.
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Set inputParams.
   */
  public function setInputParams($inputParams) {
    $this->inputParams = $inputParams;
  }

  /**
   * Get inputParams.
   */
  public function getInputParams() {
    return $this->inputParams;
  }

  /**
   * Set queryParams.
   */
  public function setQueryParams($queryParams) {
    $this->queryParams = $queryParams;
  }

  /**
   * Get queryParams.
   */
  public function getQueryParams() {
    return $this->queryParams;
  }

  /**
   * Set path.
   */
  public function setPath($path) {
    $this->path = $path;
  }

  /**
   * Get path.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Set config.
   */
  public function setConfig($config) {
    $this->config = $config;
  }

  /**
   * Get config.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Set placeholders.
   */
  public function setPlaceholders($placeholders) {
    $this->placeholders = $placeholders;
  }

  /**
   * Get placeholders.
   */
  public function getPlaceholders() {
    return $this->placeholders;
  }
}
